<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;
use Varhall\Restino\Router\RestRoute;


final class RouterFactory
{
    use Nette\StaticClass;

    public static function createRouter(): Nette\Application\IRouter
    {
        $router = new RouteList();

        $router->addRoute('web/<presenter>[/<id>]', [
            'module'    => 'Web',
            'presenter' => 'Login',
            'action'    => 'default',
            'id'        => null
        ]);

        $router[] = new RestRoute('api/<presenter>[/<id>]', [
            'module'    => 'Api',
            'presenter' => 'Default',
            'action'    => 'restList',
            'id'        => null
        ]);

        return $router;
    }
}
