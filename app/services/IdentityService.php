<?php

namespace App\Services;

use App\Models\User;
use Firebase\JWT\JWT;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Nette\Utils\DateTime;
use Nette\Utils\Random;

class IdentityService
{
    protected Cache $cache;

    protected string $key;

    protected string $algorithm;

    public function __construct(Storage $storage, string $key, string $algorithm = 'HS256')
    {
        $this->cache = new Cache($storage, 'authentication');
        $this->key = $key;
        $this->algorithm = $algorithm;
    }

    public function createAuthCode(User $user): string
    {
        $token = Random::generate(64);

        $this->cache->save($token, $user->id, [
            Cache::Expire => '5 minutes'
        ]);

        return $token;
    }

    public function createToken(User $user): string
    {
        $now = new DateTime();
        $jwt = [
            'iat'  => $now->getTimestamp(),                                                     // Issued at: time when the token was generated
            'jti'  => md5($user->id . $now->getTimestamp()),                             // Json Token Id: an unique identifier for the token
            'iss'  => $_SERVER['SERVER_NAME'],                                                  // Issuer
            'nbf'  => $now->getTimestamp(),                                                     // Not before
            'exp'  => $now->modifyClone("+14 days")->getTimestamp(),                    // Expire
            'data' => [                                                                         // Data related to the signer user
                'id'        => $user->id,
                'role'      => $user->role,
                'data'      => $user->toArray()
            ]
        ];

        return JWT::encode($jwt, $this->key, $this->algorithm);
    }

    public function getIdentity(string $token): ?User
    {
        $id = $this->cache->load($token);

        if (!$id) {
            return null;
        }

        return User::find($id);
    }
}