<?php

namespace App\Services;

class Settings
{
    protected array $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function __get(string $name)
    {
        return $this->settings[$name];
    }
}