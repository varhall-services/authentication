<?php

namespace App\Services;

use App\Models\User;

interface IProvider
{
    public function authenticate(string $username, string $password): ?User;
}