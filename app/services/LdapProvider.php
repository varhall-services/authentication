<?php

namespace App\Services;

use App\Models\User;
use LdapRecord\Connection;

class LdapProvider implements IProvider
{
    protected Connection $ldap;

    protected array $configuration;

    public function __construct(Connection $ldap)
    {
        $this->ldap = $ldap;
    }

    public function authenticate(string $username, string $password): ?User
    {
        if (!$this->ldap->auth()->attempt($username, $password)) {
            return null;
        }

        $user = $this->ldap->query()->findBy('userPrincipalName', $username);

        if (!$user) {
            return null;
        }

        return $this->getLocalUser($user);
    }

    protected function getLocalUser($ldapUser): User
    {
        $user = User::findByEmail($ldapUser['userprincipalname'][0]) ?? User::instance([
            'email'     => $ldapUser['userprincipalname'][0],
            'enabled'   => true,
            'role'      => 'user'
        ]);

        $user->fill([
            'name'      => $ldapUser['givenname'][0],
            'surname'   => $ldapUser['sn'][0],
            'password'  => null
        ]);

        return $user->save();
    }
}