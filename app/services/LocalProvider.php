<?php

namespace App\Services;

use App\Models\User;
use Nette\Security\Passwords;

class LocalProvider implements IProvider
{
    protected Passwords $passwords;

    public function __construct(Passwords $passwords)
    {
        $this->passwords = $passwords;
    }

    public function authenticate(string $username, string $password): ?User
    {
        $user = User::findByEmail($username);

        if (!$user || !$user->password || !$this->passwords->verify($password, $user->password)) {
            return null;
        }

        if (!$user->enabled) {
            return null;
        }

        return $user;
    }
}