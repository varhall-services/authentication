<?php

namespace App\Models;

use App\Repositories\UsersRepository;
use Varhall\Dbino\Model;
use Varhall\Dbino\Plugins\UuidPlugin;
use Varhall\Dbino\Traits\Timestamps;

class User extends Model
{
    use Timestamps;

    protected $casts = [
        'attributes'    => 'json',
        'enabled'       => 'bool'
    ];

    /// RELATIONS


    /// CONFIGURATION

    protected function plugins()
    {
        return [
            new UuidPlugin('id')
        ];
    }

    protected function hiddenAttributes()
    {
        return [
            'password'
        ];
    }

    protected function repository(): string
    {
        return UsersRepository::class;
    }

    public function table()
    {
        return 'users';
    }
}