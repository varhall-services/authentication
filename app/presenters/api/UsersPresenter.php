<?php

namespace App\Presenters\ApiModule;

use App\Models\User;
use Nette\Http\Response;
use Varhall\Restino\Presenters\Results\Termination;

class UsersPresenter extends ApiPresenter
{
    public function restCreate(array $data)
    {
        if (User::findByEmail($data['email'])) {
            return new Termination('Email is already registered', Response::S400_BadRequest);
        }

        return parent::restCreate($data);
    }

    protected function modelClass()
    {
        return User::class;
    }

    protected function validationDefinition()
    {
        return [
            'email'         => [ 'string:1..', 'required:only=create' ],
            'password'      => [ 'string:1..', 'required:only=create' ],
            'name'          => [ 'string:1..', 'required:only=create' ],
            'surname'       => [ 'string:1..', 'required:only=create' ],
            'role'          => [ 'string' ],
            'enabled'       => [ 'bool', 'required:only=create' ],
            'attributes'    => [ 'array' ]
        ];
    }
}