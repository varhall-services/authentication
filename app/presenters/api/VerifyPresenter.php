<?php

namespace App\Presenters\ApiModule;

use App\Presenters\BasePresenter;
use App\Services\IdentityService;
use Nette\Http\Response;
use Varhall\Restino\Presenters\Plugins\ValidatePlugin;
use Varhall\Restino\Presenters\Results\Termination;

class VerifyPresenter extends ApiPresenter
{
    protected IdentityService $service;

    public function __construct(IdentityService $service)
    {
        $this->service = $service;
    }

    public function startup()
    {
        parent::startup();

        $this->plugin(ValidatePlugin::class)->only([ 'list' ]);
    }

    public function restList(array $data = [])
    {
        if (!isset($data['auth_code'])) {
            return new Termination('Missing auth_code', Response::S400_BadRequest);
        }

        $user = $this->service->getIdentity($data['auth_code']);

        if (!$user) {
            return new Termination('Invalid auth_code', Response::S404_NotFound);
        }

        return [ 'access_token' => $this->service->createToken($user) ];
    }

    protected function methodsOnly()
    {
        return [ 'list' ];
    }

    protected function validationDefinition()
    {
        return [
            'auth_code' => [ 'string:64', 'required' ]
        ];
    }
}