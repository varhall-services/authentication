<?php

namespace App\Presenters\ApiModule;

use Varhall\Restino\Presenters\Plugins\AllowedMethodPlugin;
use Varhall\Restino\Presenters\Plugins\CorsPlugin;
use Varhall\Restino\Presenters\Plugins\DateTimePlugin;
use Varhall\Restino\Presenters\Plugins\FilterPlugin;
use Varhall\Restino\Presenters\Plugins\TransformPlugin;
use Varhall\Restino\Presenters\Plugins\ValidatePlugin;
use Varhall\Restino\Presenters\RestPresenter;
use Varhall\Securino\Authorization\Guard;
use Varhall\Securino\Authorization\Role;


/**
 * Base presenter for all application presenters.
 */
abstract class ApiPresenter extends \Nette\Application\UI\Presenter
{
    use RestPresenter;

    protected function startup()
    {
        parent::startup();

        $this->autoCanonicalize = false;

        $this->plugin(AllowedMethodPlugin::class);
        $this->plugin(CorsPlugin::class);
        //$this->plugin(AuthenticationPlugin::class);
        //$this->plugin(AuthorizationPlugin::class)->args($this->currentUser());
        $this->plugin(FilterPlugin::class)->only(['create', 'update', 'clone']);
        $this->plugin(TransformPlugin::class);
        $this->plugin(DateTimePlugin::class);
        $this->plugin(ValidatePlugin::class)->only(['create', 'update', 'clone']);

    }

    protected function getModuleName()
    {
        return strtolower(preg_replace("#:?[a-zA-Z_0-9]+$#", "", $this->getPresenter()->getName()));
    }
}
