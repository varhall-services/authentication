<?php

namespace App\Presenters\WebModule;

use App\Services\IdentityService;
use App\Services\IProvider;
use App\Services\Settings;
use Nette\Application\Attributes\Persistent;
use Nette\Application\UI\Form;
use Nette\Http\Url;

class LoginPresenter extends WebPresenter
{
    #[Persistent]
    public string $return_uri = '';

    #[Persistent]
    public string $scope = '';

    protected IdentityService $service;

    protected IProvider $provider;

    protected Settings $settings;

    public function __construct(IdentityService $service, IProvider $provider, Settings $settings)
    {
        $this->service = $service;
        $this->provider = $provider;
        $this->settings = $settings;
    }

    public function renderDefault(): void
    {
        $this->template->settings = $this->settings;
    }

    protected function authenticate(Form $form): void
    {
        $user = $this->provider->authenticate($form->getValues()->username, $form->getValues()->password);

        if ($user) {
            $token = $this->service->createAuthCode($user);

            $target = (new Url($this->return_uri))->appendQuery(array_filter([
                'scope'     => $this->scope,
                'auth_code' => $token
            ]));

            $this->redirectUrl($target->getAbsoluteUrl());

        } else {
            $form->addError('Špatné jméno nebo heslo');
        }
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();

        $form->addText('username')
            ->setRequired();

        $form->addText('password')
            ->setRequired();

        $form->onSuccess[] = function(Form $form) {
            $this->authenticate($form);
        };

        return $form;
    }
}