<?php

namespace App\Presenters\WebModule;


use Nette\Application\Helpers;
use Nette\Application\UI\TemplateFactory;

abstract class WebPresenter extends \Nette\Application\UI\Presenter
{
    public function formatTemplateFiles(): array
    {
        [, $presenter] = Helpers::splitName($this->getName());
        $presenter = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $presenter)); // convert to snake-case

        $dir = dirname(static::getReflection()->getFileName());
        $dir = is_dir("$dir/templates") ? $dir : dirname($dir);

        return array_merge(parent::formatTemplateFiles(), [
            "{$dir}/templates/{$presenter}.latte"
        ]);
    }

    public function formatLayoutTemplateFiles(): array
    {
        return array_merge(parent::formatLayoutTemplateFiles(), [
            __DIR__ . '/templates/@layout.latte'
        ]);
    }
}