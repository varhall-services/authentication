<?php

namespace App\Repositories;

use App\Models\User;
use Nette\Security\Passwords;
use Varhall\Dbino\Events\SaveArgs;
use Varhall\Dbino\Repository;

class UsersRepository extends Repository
{
    /** @var Passwords */
    protected $passwords;

    public function __construct(Passwords $passwords)
    {
        $this->passwords = $passwords;

        $this->on('saving', function(SaveArgs $args) {
            if (isset($args->data['password'])) {
                $args->instance->password = $this->passwords->hash($args->data['password']);
            }
        });
    }

    public function findByEmail(string $email): ?User
    {
        return $this->where('email', $email)->first();
    }
}