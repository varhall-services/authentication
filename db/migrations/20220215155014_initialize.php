<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Initialize extends AbstractMigration
{
    public function change(): void
    {
        $this->table('users', [ 'id' => false, 'primary_key' => 'id' ])
            ->addColumn('id', 'char', [ 'length' => 36 ])
            ->addColumn('email', 'string')
            ->addColumn('password', 'string', [ 'null' => true ])
            ->addColumn('name', 'string')
            ->addColumn('surname', 'string')
            ->addColumn('role', 'string', [ 'null' => true ])
            ->addColumn('enabled', 'boolean')
            ->addColumn('attributes', 'json', [ 'null' => true ])
            ->addTimestamps()
            //->addColumn('deleted_at', 'timestamp', [ 'null' => true ])
            ->addIndex('email', [ 'unique' => true ])
            ->create();
    }
}
