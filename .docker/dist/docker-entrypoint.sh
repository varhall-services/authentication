#!/usr/bin/env bash

set -e

if [ "$1" = 'apache2-foreground' ]; then
    shopt -s dotglob

    # Transfer ENV variables to cron ENV - important!
    printenv | grep -v "no_proxy" >> /etc/environment

    cd /var/www/html

    # Setup permissions
    chown -R www-data:www-data temp
    chmod 777 temp

    chown -R www-data:www-data log
    chmod 777 log

    # Run database migrations
    rm -rf temp/cache
fi

exec "$@"