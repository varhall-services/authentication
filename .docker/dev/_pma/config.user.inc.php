<?php

// Custom configuration file
// Documentation: https://docs.phpmyadmin.net/en/latest/index.html

$cfg['MaxNavigationItems'] = 10000;
$cfg['LoginCookieValidity'] = 100000000;

ini_set('session.gc_maxlifetime', 100000000);
